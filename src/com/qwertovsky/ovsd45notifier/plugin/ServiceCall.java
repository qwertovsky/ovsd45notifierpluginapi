package com.qwertovsky.ovsd45notifier.plugin;

import java.util.List;
import java.util.Map;


/**
 * <br /> personID  - номер заявителя
<br /> personName - имя заявителя
<br /> personUrl  - адрес страницы заявителя
<br /> personEmail - почта заявителя
<br /> personPhone  - телефон заявителя
<br /> personOrgID - номер отдела заявителя
<br /> personOrgName - имя отдела заявителя
<br /> personOrgUrl - адрес страницы отдела заявителя
<br /> personJobTitle - должность заявителя
<br /> personManagerName - имя руководителя заявителя
<br /> personManagerID - номер руководителя заявителя
<br /> personManagerUrl - адрес страницы руководителя заявителя
<br /> regDate - дата регистрации
<br /> impact - влияние
<br /> priority - приоритет
<br /> description - краткое описание
<br /> problem - проблема (краткое описание + подробное описание)
<br /> attachments - вложения
<br /> assignedUserName - назначенный исполнитель
<br /> workHistory - ход решения
<br /> mustApprove - логическая (отправлено на согласование)
<br /> approvalIniciatorName - инициатор согласования
<br /> approvalIniciatorID - номер инициатора согласования
<br /> approvalDeadline - крайний срок согласования
<br /> approvalDescription - описание согласования
<br /> approvalComment - комментарий согласования
<br /> approved - логическая (одобрено)
<br /> notApproved - логическая (отклонена)
<br /> approvalResult - результат согласования (текстовая)
<br /> approvalVotes - список (согласующие)
<br /> ready - логическая (работы по Вашей заявке выполнены)
<br /> scUrl - адрес заявки в веб-интерфейсе)
 *
 */
public interface ServiceCall
{
	/**
	 * System Object ID
	 */
	public String OID();
	public String ID();
	public String status();
	public String regDate();
	public String description();
	public String assignedUserName();
	public String workGroupName();
	public String serviceName();
	public String problem();
	public String workHistory();
	public String impact();
	public String priority();
	public String personID();
	public String personName();
	public String personImageUrl();
	public String personUrl();
	public String personEmail();
	public String personPhones();
	public String personJobTitle();
	public String personManagerName();
	public String personManagerID();
	public String personManagerUrl();
	public String personLocation();
	public String personDepartment();
	public String personDepartmentID();
	public String personDepartmentUrl();
	public String personAddress();
	public Map<String, String> attachments();
	public String approvalIniciatorName();
	public String approvalIniciatorID();
	public String approvalDescription();
	public String approvalComment();
	public String approvalDeadline();
	public String approvalResult();
	public List<ApprovalVote> approvalVotes();
	public String dataformScript();
	public String lastNote();
	public String lastEditPerson();
	public boolean isCaller();
	public boolean isAssigned();
}
