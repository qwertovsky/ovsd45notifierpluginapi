package com.qwertovsky.ovsd45notifier.plugin;

import org.apache.log4j.Logger;
import org.eclipse.swt.widgets.Menu;

/**
 * Plugin must implements this interface.
 * <br />
 * Plugin can get all data from service call and  add menu to tray icon.
 * 
 */
public interface Plugin
{	
	/**
	 * Add items to menu of tray icon.<br />
	 * Example:
	 * <pre>
	 * public void createMenuItem(Menu parent)	throws Exception
	 * {
	 * 	MenuItem menuItem = new MenuItem(parent, SWT.CHECK);
	 *	menuItem.setText(getName());
	 *	menuItem.setSelection(isEnabled());
	 *	menuItem.addSelectionListener(new SelectionAdapter()
	 *		{
	 *			public void widgetSelected(SelectionEvent event)
	 *			{
	 *				MenuItem item = ((MenuItem)event.getSource());
	 *				setEnabled(item.getSelection());
	 *			}
	 *		});
	 *	}
	 * </pre>
	 * @param parent
	 * @throws Exception
	 */
	public void createMenuItem(Menu parent)	throws Exception;
	
	/**
	 * Method for process service call.<br />
	 * Service call put to all plugins. If it's not need to show notification for service call, 
	 * function must return FALSE value. It is not stop processing of other plugins. 
	 * @param serviceCall
	 * @return show notification for this service call or not
	 * @throws Exception
	 */
	public boolean processServiceCall(ServiceCall serviceCall) throws Exception;

	
	/**
	 * If plugin is not enabled, {@link #processServiceCall(ServiceCall)} is not invoked. 
	 * @return boolean (enabled or not)
	 */
	public boolean isEnabled();

	/**
	 * 
	 * @return name of plugin
	 */
	public String getName();
	
	/**
	 * Plugin can get logger of application.
	 * @param logger
	 */
	public void setLogger(Logger logger);
	
}