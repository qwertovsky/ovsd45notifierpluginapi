package com.qwertovsky.ovsd45notifier.plugin;


/**
 * Иформация по согласованию заявки
 *
 */
public interface ApprovalVote
{
	public String getPersonID();

	public String getPersonName();

	public String getPersonUrl();

	public boolean isApproved();

	public String getReason();
}
